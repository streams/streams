<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
	<ShortName>{{$project}}</ShortName>
	<Description>{{$search_project}}</Description>
	<Image height="64" width="64" type="image/png">{{$photo}}</Image>
	<Url type="text/html" template="{{$searchurl}}"/>
	<Url type="{{$aptype}}" template="{{$searchurl}}"/>
</OpenSearchDescription>

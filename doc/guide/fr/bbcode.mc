Guide du BBcode
===============

[h3]Mise en forme du texte[/h3]
[table]
[tr]
[th]Syntaxe BBcode[/th][th]Rendu du texte[/th]
[/tr]
[tr]
[td][nobb][b]gras[/b][/nobb][/td][td]<strong>gras</strong>[/td]
[/tr]
[tr]
[td][nobb][i]italique[/i][/nobb][/td][td]<em>italique</em>[/td]
[/tr]
[tr]
[td][nobb][u]souligné[/u][/nobb][/td][td]<u>souligné</u>[/td]
[/tr]
[tr]
[td][nobb][s]barré[/s][/nobb][/td][td]<strike>barré</strike>[/td]
[/tr]
[tr]
[td][nobb]exposant[sup]script[/sup][/nobb][/td][td]exposant<sup>script</sup>[/td]
[/tr]
[tr]
[td][nobb]indice[sub]script[/sub][/nobb][/td][td]indice<sub>script</sub>[/td]
[/tr]
[tr]
[td][nobb][color=red]rouge[/color][/nobb][/td][td]<span style="color: red;">rouge</span>[/td]
[/tr]
[tr]
[td][nobb][hl]surligné[/hl][/nobb][/td][td]<span style="background-color: yellow;">surligné</span>[/td]
[/tr]
[tr]
[td][nobb][font=courier]une autre police[/font][/nobb][/td][td]<span style="font-family: courier;">une autre police</span>[/td]
[/tr]
[tr]
[td][nobb][quote]citation[/quote][/nobb][/td][td]<blockquote>citation</blockquote>[/td]
[/tr]
[tr]
[td][nobb][quote=Bidule]Truc, machin, chose...[/quote][/nobb][/td]
[td]<strong class="author">Bidule wrote:</strong><blockquote>Truc, machin, chose...</blockquote>[/td]
[/tr]
[tr]
[td][nobb][size=small]Texte 'small'[/size]&nbsp;
[size=xx-large]Texte 'xx-large'[/size]&nbsp;
[size=20]Précisément 20px[/size]&nbsp'
[/nobb]
Tailles disponibles: [b]xx-small, small, medium, large, xx-large[/b][/td]
[td]<span style="font-size: small;">Texte 'small'</span><br><span style="font-size: xx-large;">Texte 'xx-large'</span><br><span style="font-size: 20px;">Précisément 20px</span>[/td]
[/tr]
[tr]
[td][nobb]Ajouter une barre horizontale
[hr]
Comme ceci[/nobb][/td]
[td]Ajouter une barre horizontale<br><hr><br>Comme ceci[/td]
[/tr]
[tr]
[td][nobb]Ceci est du
[center]texte centré[/center]
en BBcode[/nobb][/td]
[td]
Ceci est du<br><div style="text-align:center;">texte centré</div><br>en BBcode
[/td]
[/tr]
[/table]

<h3>Blocs de code</h3>
L'affichage du code peut être rendu sous forme de bloc ou en ligne (selon que le texte contient ou non des caractères de retour à la ligne). Vous pouvez également spécifier un langage pris en charge pour une mise en évidence syntaxique améliorée. La mise en évidence de la syntaxe nécessite un addon complémentaire de rendu approprié. De cet addon dépendront les langages pris en charge, lesquels [i]peuvent[/i] inclure <strong>php, css, mysql, sql, abap, diff, html, perl, ruby, vbscript, avrc, dtd, java, xml, cpp, python, javascript, js, json, sh </strong>.

Si un addon complémentaire de rendu n'est pas installé ou si un langage non prise en charge est spécifié, l'affichage du bloc de code sera le même que celui de la balise de bloc de code par défaut.

[table]
[tr]
[th]Syntaxe BBcode[/th][th]Rendu[/th]
[/tr]
[tr]
[td][nobb][code]function bbcode() { }[/code][/nobb][/td][td]<code>function bbcode() { }</code>[/td]
[/tr]
[tr]
[td][nobb][code=php]
function bbcode() {
  $variable = true;
  if ( $variable ) {
	echo "true";
  }
}
[/code][/nobb][/td]
[td]<code>
function bbcode() {
  $variable = true;
  if ( $variable ) {
	echo "true";
  }
}
</code>[/td]
[/tr]
[tr]
[td][nobb][nobb]Voici comment [i]vous[/i]
pouvez [u]montrer[/u] comment 
utiliser la syntaxe [hl]BBcode[/hl][/nobb][/nobb][/td]
[td][nobb]Voici comment [i]vous[/i] pouvez [u]montrer[/u] comment utiliser la syntaxe [hl]BBcode[/hl][/nobb][/td]
[/tr][/table]

[h3]Listes[/h3]
[table]
[tr]
[th]Syntaxe BBcode[/th][th]Rendu de la liste[/th]
[/tr]
[tr]
[td][nobb]
[ul]\
[*] Premier item de la liste
[*] Deuxième item de la liste
[/ul][/nobb][/td]
[td]<ul class="listbullet" style="list-style-type: circle;"><li> Premier item de la liste</li><li> Deuxième item de la liste<br></li></ul>[/td]
[/tr]
[tr]
[td][nobb]
[ol]\
[*] Premier item de la liste
[*] Deuxième item de la liste
[/ol][/nobb][/td]
[td]<ul class="listdecimal" style="list-style-type: decimal;"><li> Premier item de la liste</li><li> Deuxième item de la liste<br></li></ul>[/td]
[/tr]
[tr]
[td][nobb]
[list=A]\
[*] Premier item de la liste
[*] Deuxième item de la liste
[/list][/nobb]

Les options de liste sont 1, i, I, a, A.[/td]
[td]<ul class="listupperalpha" style="list-style-type: upper-alpha;"><li> Premier item de la liste</li><li> Deuxième item de la liste</li></ul>[/td]
[/tr]
[/table]

[h3]Tableaux[/h3]

[table]
[tr]
[th]Syntaxe BBcode[/th][th]Rendu du tableau[/th]
[/tr]
[tr]
[td][nobb][table border=0]\
[tr][th]En-tête 1[/th][th]En-tête 2[/th][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[/table][/nobb]
[/td]
[td]<table class="table"><tbody><tr><th>En-tête 1</th><th>En-tête 2</th></tr>
<tr><td>Contenu</td><td>Contenu</td></tr><tr><td>Contenu</td><td>Contenu</td></tr></tbody></table>
[/td]
[/tr]
[tr]
[td][nobb][table border=1]\
[tr][th]En-tête 1[/th][th]En-tête 2[/th][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[/table][/nobb][/td]
[td]<table class="table table-bordered"><tbody><tr><th>En-tête 1</th><th>En-tête 2</th></tr>
<tr><td>Contenu</td><td>Contenu</td></tr><tr><td>Contenu</td><td>Contenu</td></tr></tbody></table>
[/td]
[/tr]
[tr]
[td][nobb][table]\
[tr][th]En-tête 1[/th][th]En-tête 2[/th][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[tr][td]Contenu[/td][td]Contenu[/td][/tr]\
[/table][/nobb][/td]
[td]<table class="table"><tbody><tr><th>En-tête 1</th><th>En-tête 2</th></tr>
<tr><td>Contenu</td><td>Contenu</td></tr><tr><td>Contenu</td><td>Contenu</td></tr></tbody></table>
[/td]
[/tr]
[/table]

<h3>Liens et contenu intégré</h3>

[table]
[tbody]
[tr][th]Syntaxe BBcode[/th][th]Rendu[/th][/tr]
[tr][td][nobb][video]URL de la vidéo[/video]<br>
[video poster="image.jpg"]URL de la vidéo[/video]<br>
[audio]URL du fichier audio[/audio]<br>[/nobb][/td]
[td][/td][/tr]
[tr][td][nobb][url=https://codeberg.org/streams]Code[/url][/nobb][/td]
[td]<a href="https://codeberg.org/streams" target="_blank">Code</a>[/td]
[/tr]
[tr]
[td][nobb]Une image
[img]https://example.org/image.jpg[/img]
dans du texte [/nobb][/td]
[td]Une image<br><img src="[baseurl]/images/default_profile_photos/rainbow_man/300.jpg" style="height: 75px; width:75px;" alt="Image/photo"><br>dans du texte[/td]
[/tr]
[tr]
[td][nobb]Une image avec texte alternatif
[img alt="Description de l'image"]https://example.org/image.jpg[/img][/nobb][/td]
[td]Une image avec texte alternatif<br><img src="[baseurl]/images/default_profile_photos/rainbow_man/300.jpg" style="width:75px; width:75px;" title="photo description" alt="Description de l'image">[/td]
[/tr]

[/tbody]
[/table]
	

<h3>Balises spécifiques à $Projectname</h3>

[table]
[tbody]
[tr][th]Syntaxe BBcode[/th][th]Rendu[/th][/tr]
[tr][td][nobb]Version Magic-auth de la balise [url]
[zrl=https://macgirvin.com]Lien prenant en compte l'identité[/zrl][/nobb][/td]
[td][/td][/tr]
[tr]
[td]Version Magic-auth de la balise [img]
[nobb][zmg]https://hubzilla.org/une/photo.jpg[/zmg][/nobb]
[/td][td]L'image n'est visible que par les personnes authentifiées et autorisées.[/td]
[/tr]
[tr]
[td]Rendu lié à l'observateur:
[nobb][observer=1]Texte à afficher si l'observateur EST authentifié[/observer][/nobb]
[/td][td][/td]
[/tr]
[tr]
[td]
[nobb][observer=0]Texte à afficher si l'observateur N'EST PAS authentifié[/observer][/nobb][/td]
[td][/td]
[/tr]
[tr]
[td][nobb][observer.language=en]Texte à afficher si l'observateur est anglophone[/observer][/nobb][/td]
[td][/td]
[/tr]
[tr]
[td][nobb][observer.language!=fr]Texte à afficher si l'observateur n'est pas francophone[/observer][/nobb][/td]
[td][/td]
[/tr]
[tr]
[td][nobb][observer.url][/nobb][/td]
[td]URL du canal de l'observateur[/td]
[/tr]
[tr]
[td][nobb][observer.baseurl][/nobb][/td]
[td]Site web de l'observateur[/td]
[/tr]
[tr]
[td][nobb][observer.name][/nobb][/td]
[td]Nom de l'observateur[/td]
[/tr]
[tr]
[td][nobb][observer.webname][/nobb][/td]
[td]Pseudo visible dans l'URL de l'observateur[/td]
[/tr]
[tr]
[td][nobb][observer.address][/nobb][/td]
[td]Identité de l'observateur sur le Fediverse[/td]
[/tr]
[tr]
[td][nobb][observer.photo][/nobb][/td]
[td]Photo de profil de l'observateur[/td]
[/tr]
[tr]
[td][nobb]C'est quoi un "spoiler"??
[spoiler]Texte que vous voulez masquer.[/spoiler][/nobb][/td]
[td]
C'est quoi un "spoiler"?? <div onclick="openClose('opendiv-1131603764'); return false;" class="fakelink">Click to open/close</div><blockquote id="opendiv-1131603764" style="display: none;">Texte que vous voulez masquer.</blockquote>[/td]
[/tr]
[tr]
[td][nobb][rpost=Titre]Text to post[/rpost][/nobb]
L'observateur sera renvoyé à son site d'origine pour saisir un message avec le titre et le contenu spécifiés. Ces deux éléments sont facultatifs[/td]
[td]<a href="[baseurl]/rpost?f=&amp;title=Titre&amp;body=Texte+%C3%A0+publier" target="_blank">[baseurl]/rpost?f=&amp;title=Titre&amp;body=Texte+%C3%A0+publier</a>[/td]
[/tr]
[tr]
[td]Génrer un QR code
Ceci requiert l'addon <strong>qrator</strong>.
[nobb][qr]Texte à publier[/qr][/nobb][/td]
[td][/td]
[/tr]
[tr]
[td]Ceci requiert un addon de cartographie tel que <strong>openstreetmap</strong>.
[nobb][map][/nobb][/td]
[td]Générer une carte dans la publication en utilisant les coordonnées actuelles, si la localisation du navigateur de l'auteur est activée.[/td]
[/tr]
[tr]
[td]Ceci requiert un addon de cartographie tel que <strong>openstreetmap</strong>.
[nobb][map=latitude,longitude][/nobb][/td]
[td]Générer une carte à l'aide de coordonnées géographiques.[/td][/tr]
[tr]
[td]Ceci requiert un addon de cartographie tel que <strong>openstreetmap</strong>.
[nobb][map]Nom du lieu[/map][/nobb][/td]
[td]
Générer une carte pour un lieu donné identifié par son nom. Le premier lieu correspondant est renvoyé. Par exemple, "Paris" renverra généralement Paris, France, et non Paris, Texas, États-Unis, à moins que l'emplacement ne soit spécifié de façon plus précise. Il est fortement recommandé d'utiliser le bouton de prévisualisation de publication pour s'assurer que la localisation est correcte avant de publier.
[/td]
[/tr]
[tr]
[td][nobb][&amp;&ZeroWidthSpace;copy;][/nobb][/td]
[td] &copy; [/td]
[/tr]
[/tbody][/table]


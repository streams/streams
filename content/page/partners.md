---
title: Partners
menu:
  main:
    name: "partners"
    weight: 4
layout: article
---

{{% modulel image_path="/img/logoIdF.jpg" title="" width="350" description="Through its DIM Origines program, Region Ile-de-France is supporting about 60% of associated hardware implementation costs"%}} 

{{% modulel image_path="/img/cnrs+thales.jpg" title="" width="350" description="Additional sponsors include CNRS and Thales LAS France. Both provide in-kind sponsorship as well as cash contributions. On top of this hosting costs for the platform at IDRIS (including rack space, power supply, cooling) will be covered by CNRS-INSU"%}}  

Several vendors are also supporting the project through significant hardware donations

 ![vendors](/img/vendors-logo.jpg)

- NVIDIA has donated 2x A100x DPUs and a SN3700 switch 
- Graphcore has donated 2x BOW 2000 IPU-Machine
- AMD has donated 2x Genoa 9654 96c CPU and 1x Instinct Mi210 GPU
- REFLEX CES has donated 1x Intel Stratix 10 GX H-Tile board



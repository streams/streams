---
title: Publications
comments: false
layout: article
aside:
  toc: true
---

# COSMIC in international conferences

- [*Cetre, C. et al.*,"Real-time high performance computing using a Jetson Xavier AGX", ERTS 2022](https://hal.science/hal-03693764/document)
- [*Ferreira, F. et al.*,"COSMIC: a real-time platform for signal processing pipelines", SiPS 2022](https://doi.org/10.1109/SiPS55645.2022.9919251)
- [*Gratadour, D. et al.*,"MAVIS RTC: a forward looking implementation of the COSMIC platform", SPIE 2022](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/12185/121850T/MAVIS-RTC--a-forward-looking-implementation-of-the-COSMIC/10.1117/12.2629956.short)
- [*Sevin, A. et al.*,"The MICADO first light imager for the ELT: final design and prototype of the MICADO SCAO RTC", SPIE 2022](https://doi.org/10.1117/12.2627224)
- [*Engler, B. et al.*,"The GPU-based High-order adaptive OpticS Testbench", SPIE 2022](https://doi.org/10.1117/12.2630595)
- [*Chin, J. et al.*,"Keck adaptive optics facility: real time controller upgrade", SPIE 2022](https://doi.org/10.1117/12.2629614)
- [*Boccaletti, A. et al.*,"Upgrading the high contrast imaging facility SPHERE: science drivers and instrument choices", SPIE 2022](https://doi.org/10.1117/12.2630154)
- [*Plante, J. et al.*,"A high-performance data acquisition on COTS hardware for astronomical instrumentation", SPIE 2022](https://doi.org/10.1117/12.2627827)
- [*Gratadour, D. et al.*,"MAVIS real-time control system: a high-end implementation of the COSMIC platform", SPIE 2020](https://doi.org/10.1117/12.2562082)
- [*Biasi, R. et al.*, "Implementation and initial test results of the new Keck real time controller", SPIE 2020](https://doi.org/10.1117/12.2312593)
- [*Ferreira, F. et al.*, "Hard real-time core software of the AO RTC COSMIC platform: architecture and performance", SPIE 2020](https://doi.org/10.1117/12.2561244)




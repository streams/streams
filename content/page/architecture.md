---
title: Architecture
menu:
  main:
    name: "architecture"
    weight: 2
layout: article
---

The platform will be built around a “backbone” (high-speed, low-latency network switch), which will be used to route the data through the platform and to which the 3 main subsystems will be connected:

 ![archi](/img/streams-archi.png)
 
- the data emulation subsystem, allowing to simulate the production of data by sensors (radio-telescope), assembled on the basis of a high-performance computing server, making it possible to accommodate high-speed network controllers and/or FPGA boards;
- the real-time ingestion & processing subsystem, composed of several high-performance servers, equipped with high-speed smart network controllers (ingest) and a high density GPU cluster (or future hardware accelerators). These servers will be designed to be scalable in order to test new computing technologies (GPU in the initial phase, then more heterogeneous configuration: GPU + FPGA + other);
- the supervision and training subsystem, made up of high-performance servers equipped with network controllers for capturing flows of data passing through the platform, as well as a very dense integrated cluster of GPUs or specialized AI processors (for deep learning).
These subsystems will also be equipped with significant high-speed / low-latency storage capacities.



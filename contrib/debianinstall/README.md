**This installation script was provided by the community and is officially unsupported. Use at your own risk. Only use with a fresh install of Debian stable. If you have a server that has Apache, PHP, MySQL, SSL, etc. already installed this will break your server.**

--

# How to use

### Keep it Simple and Stupid

The script keeps everything as simple as possible (KISS):

- Apache as webserver (there is no choice to use another webserver like nginx)
- default PHP version of Debian
- one single Streams intance only
- re-running the script does no harm

### When to use other Scripts

Use the scripts under [easyinstall](https://codeberg.org/streams/streams/src/branch/release/contrib/autoinstall) or [autoinstall](https://codeberg.org/streams/streams/src/branch/release/contrib/autoinstall) if you look for more choices. The main differences are:

- Apache OR nginx as webserver
- install multiple instances (domains) that run side by side on the server
- adds apache vhosts (instead of using the standard doc root /var/www/streams)
- install PHP from https://packages.sury.org/php/ (instead of using the Debian repository)
- graphical installer whiptail
- The script stops (fails) if it finds results of a previous installation.
- If something fails the script tries to clean up everything that was installed up to the point of failure. (That might cause trouble if certbot registered a certificate already.)

## Preconditions

Hardware

+ internet connection and router at home
+ computer connected to your router (a Raspberry 4 will do for very small instances)

Software

+ fresh installation of Debian 12 (bookworm)
+ router with open ports 80 and 443 for your web server

You can of course run the script on a VPS or any distant server as long as the above sotfware requirements are satisfied.

## How to run the script

+ Register your own domain (for example at selfHOST) or a free subdomain (for example at freeDNS)
+ Log on to your fresh Debian
  - apt-get install git
  - mkdir -p /var/www
  - cd /var/www
  - git clone https://codeberg.org/streams/streams.git
  - cd streams/contrib/debianinstall
  - cp config.txt.template config.txt
  - nano config.txt
    - read the comments carefully
    - enter your values: db pass, domain
    - (optionally) Enter your values for dyn DNS
  - ./debian-setup.sh as root
    - ... wait, wait, wait until the script is finished
+ Open your domain with a browser and step through the initial configuration of your instance.
  - default database name = streams
  - default dababase user = streams

## Optional - Switch verification of email on/off

Do this just before you register the first user without email verification.

In a terminal

    su -
    cd /var/www/streams

Check the current setting 

    util/config system verify_email

Switch the verification off

    util/config system verify_email 0

## What the script will do for you...

+ install everything required by your streams instance, basically a web server (Apache), PHP, a database (MySQL/MariaDB), certbot,...
+ create a database
+ run certbot to have everything for a secure connection (httpS)
+ create a script for daily maintenance
  - renew certfificate (letsencrypt)
  - update of your instance for core and addons (git)
  - update of Debian
  - restart
+ create cron jobs for
  - DynDNS (selfHOST.de or freedns.afraid.org) every 5 minutes
  - Run.php for your instance every 10 minutes
  - daily maintenance script every day at 05:30

The script is known to work without adjustments with

+ Hardware
  - standard PC with Debian 12 (bookworm)
+ DynDNS
  - selfHOST.de
  - freedns.afraid.org

# Step-by-Step - some Details

## Preparations

## Configure your Router

Your webserver has to be visible in the internet.  

Open the ports 80 and 443 on your router for your Debian. Make sure your web server is marked as "exposed host".

## Preparations Dynamic IP Address

Follow the instructions in contrib/debianinstall/config.txt.  

In short...  

Your streams server must be reachable by a domain that you can type in your browser

    cooldomain.org

You can use subdomains as well

    my.cooldomain.org

There are two ways to get a domain...

### Method 1: Buy a Domain 

...for example buy at selfHOST.de  

The cost is 1,00 € per month (2024).

### Method 2: Register a free subdomain

...for example register at freedns.afraid.org

## Note on Rasperry 

It is recommended to run the Raspi without graphical frontend (X-Server). Use...

    sudo raspi-config

to boot the Rapsi to the client console.

DO NOT FORGET TO CHANGE THE DEFAULT PASSWORD FOR USER PI!


Greetings friends. So glad you found us. 

Are you ready to take control of your social media existence and use it constructively? 

What is provided here is an open source fediverse server with a long history of innovation. The primary focus is on privacy, consent, resilience, and the corresponding improvements in online safety that this provides. 

See [FEATURES](https://codeberg.org/streams/streams/src/branch/dev/FEATURES.md).

This software supports a wide range of online behaviour, from personal communications with restricted media access - all the way to fully public broadcasting. We're big on offering choice; and giving you full control of how you wish to interact and whom you wish to interact with online. 

The default settings tend to favor personal and private use on relatively modest hardware for close friends and family. They also provide what we believe is the most harassment-free experience for marginalised people and communities available today in the fediverse - "out of the box". Feel free to adjust these if online safety isn't your thing. 

This repository uses a community-driven model. This means that there are no dedicated developers working on new features or bug fixes or translations or documentation. Instead, it relies on the contributed efforts of those that choose to use it.

This work is dedicated to the public domain to the extent permissible by law and **is not associated with any consumer brand or product**.

### Resources

A fediverse support group exists at 
https://fediversity.site/channel/streams

Self-hosted installation is covered in
https://codeberg.org/streams/streams/src/branch/release/install/INSTALL.txt

A list of sites that permit registration (approval may be required) is located at
https://fediversity.site/communities?type=streams_repository&open=1

### Third party resources
(please refer to these locations for instruction and support):

A docker image is maintained at
https://codeberg.org/node9/streams.docker
https://codeberg.org/7t4/streams/src/branch/docker/contrib/docker

A Yunohost install is present in
https://apps.yunohost.org/app/streams

Language translation instructions are provided in
https://codeberg.org/streams/streams/src/branch/release/util/README


### A brief overview of the streams repository.

The streams repository is a fediverse server with a long history. It began in 2010 as a decentralised Facebook alternative called Mistpark. It has gone through a number of twists and turns in its long journey of providing federated communications. The fediverse servers Friendica and Hubzilla are early branches of this repository.

The first thing to be aware of when discussing the streams repository is that it has no brand or brand identity. None. The name is the name of a code repository. Hence "the streams repository". It isn't a product. It's just a collection of code which implements a fediverse server that does some really cool stuff. There is no flagship instance. There is no mascot. In fact all brand information has been removed. You are free to release it under your own brand. Whatever you decide to call your instance of the software is the only brand you'll see. The software is in the public domain to the extent permissable by law.  There is no license.

If you look for the streams repository in a list of popular fediverse servers, you won't find it. We're not big on tracking and other spyware. Nobody knows how many instances there are or how many Monthly Active Users there are. These things are probably important to corporations considering takeover targets. They aren't so important to people sharing things with friends and family.

Due to its origins as a Facebook alternative, the software has a completely different focus than those fediverse projects modelled after Twitter/X. Everything is built around the use of permissions and the resulting online safety that permissions-based systems provide. Comment controls are built-in. Uploaded media and document libraries are built-in and media access can be restricted with fine-grained permissions - as can your posts. Groups are built-in. "Circles" are built-in. Events are built-in. Search and search permissions? Yup. Built-in also. It's based on Opensearch. You can even search from your browser and find anything you have permission to search for.  Spam is practically non-existent. Online harrassment and abuse are likewise almost non-existent. Moderation is a built-in capability. If you're not sure about a new contact, set them to moderated, and you'll have a chance to approve all of their comments to your posts before those comments are shared with your true friends and family. For many fediverse projects, the only way to control this kind of abusive behaviour is through blocking individuals or entire websites. The streams repository offers this ability as well. You'll just find that you hardly ever need to use it.

Because federated social media is a different model of communications based on decentralisation, cross-domain single sign-on is also built-in. All of the streams instances interact cooperatively to provide what looks like one huge instance to anybody using it - even though it consists of hundreds of instances of all sizes.

Nomadic identity is built-in. You can clone your identity to another instance and we will keep them in sync to the best of our ability. If one server goes down, no big deal. Use the other. If it comes back up again, you can go back. If it stays down forever, no big deal. All of your friends and all your content are available on any of your cloned instances.  So are your photos and videos, and so are your permission settings. If you made a video of the kids to share with grandma (and nobody else), grandma can still see the video no matter what instance she accesses it from. Nobody else can.

Link your scattered fediverse accounts and their separate logins, with one channel and one identity to share them all. 

Choose from our library of custom filters and algorithms if you need better control of the stuff that lands in your stream. By default, your conversations are restricted to your friends and are not public. You can change this if you want, but this is the most sensible default for a safe online experience.

There are no inherent limits to the length of posts or the number of photos/videos you can attach or really any limits at all. You can just share stuff without concerning yourself with any of these arbitrary limitations.

Need an app? Just visit a website running the streams repository code and install it from your browser.  

Nobody is trying to sell you this software or aggressively convince you to use it. What we're trying to do is show you through our own actions and example that there are more sensible ways to create federated social networks than what you've probably experienced.

You can find us at https://codeberg.org/streams/streams.

A support group is provided at https://fediversity.site/channel/streams (streams@fediversity.site).

Have a wonderful day.

Mike Macgirvin 🖥️
<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Location extends BaseObject
{
    public $host;
    public $address;
    public $id_url;
    public $primary;
    public $url;
    public $url_sig;
    public $site_id;
    public $callback;
    public $sitekey;
    public $deleted;
    public $driver;

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Location
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Location
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUrl()
    {
        return $this->id_url;
    }

    /**
     * @param mixed $id_url
     * @return Location
     */
    public function setIdUrl($id_url)
    {
        $this->id_url = $id_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param mixed $primary
     * @return Location
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Location
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlSig()
    {
        return $this->url_sig;
    }

    /**
     * @param mixed $url_sig
     * @return Location
     */
    public function setUrlSig($url_sig)
    {
        $this->url_sig = $url_sig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * @param mixed $site_id
     * @return Location
     */
    public function setSiteId($site_id)
    {
        $this->site_id = $site_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param mixed $callback
     * @return Location
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSitekey()
    {
        return $this->sitekey;
    }

    /**
     * @param mixed $sitekey
     * @return Location
     */
    public function setSitekey($sitekey)
    {
        $this->sitekey = $sitekey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     * @return Location
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param mixed $driver
     * @return Location
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
        return $this;
    }



}

<?php

namespace Code\Update;

class _1281
{
    public function run()
    {
        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            return UPDATE_SUCCESS;
        }
        else {
            q("START TRANSACTION");
            $r1 = q("ALTER TABLE item MODIFY COLUMN obj MEDIUMTEXT NOT NULL");
            $r2 = q("ALTER TABLE item MODIFY COLUMN target MEDIUMTEXT NOT NULL");
            $r = $r1 && $r2;
            if ($r) {
                q("COMMIT");
                return UPDATE_SUCCESS;
            }

            q("ROLLBACK");
            return UPDATE_FAILED;
        }

    }

    public function verify()
    {
        $columns = db_columns('item');
        return in_array('obj', $columns);
    }


}

<?php

namespace Code\Module;

use Code\Lib\ActorId;
use Code\Web\Controller;
use Code\Lib\ActivityStreams;
use Code\Lib\Activity as ZlibActivity;
use Code\Lib\Libzot;
use Code\Web\HTTPSig;
use Code\Lib\ThreadListener;
use Code\Lib\Channel;
use App;

class Conversation extends Controller
{

    public function init()
    {
        $contextHistory = false;
        if (ActivityStreams::is_as_request() || Libzot::is_nomad_request()) {
            if (argc() > 2 && argv(1) === 'history') {
                $contextHistory = true;
                $item_id = argv(2);
            }
            else {
                $item_id = argv(1);
            }

            if (!$item_id) {
                http_status_exit(404, 'Not found');
            }

            $portable_id = EMPTY_STR;

            $item_normal = " and item.item_hidden = 0 and item.item_type = 0 and item.item_unpublished = 0 and item.item_delayed = 0 and item.item_blocked = 0 and not verb in ( 'Follow', 'Ignore' ) ";

            $i = null;

            // do we have the item (at all)?

            $test = q(
                "select * from item where mid like '%s' and mid like '%s' $item_normal limit 1",
                dbesc(z_root() . '%'),
                dbesc('%/activity/' . $item_id)
            );

            if (!$test) {
                $test = q(
                    "select * from item where mid like '%s' and mid like '%s' $item_normal limit 1",
                    dbesc(z_root() . '%'),
                    dbesc('%/item/' . $item_id)
                );
                if (!$test) {
                    http_status_exit(404, 'Not found');
                }
            }
            // process an authenticated fetch
            $sigdata = HTTPSig::verify(EMPTY_STR);
            if ($sigdata['portable_id'] && $sigdata['header_valid']) {
                $actorId = new ActorId($sigdata['portable_id']);
                $portable_id = $actorId->getId();
                if (!check_channelallowed($portable_id)) {
                    http_status_exit(403, 'Permission denied');
                }
                if (!check_siteallowed($sigdata['signer'])) {
                    http_status_exit(403, 'Permission denied');
                }
                q("update abook set abook_archived = 0 where abook_archived = 1 and abook_xchan = '%s'",
                    dbesc($portable_id)
                );
                observer_auth($portable_id);
                // first see if we have a copy of this item's parent owned by the current signer
                // include xchans for all zot-like networks - these will have the same guid and public key

                $x = q(
                    "select * from xchan where xchan_hash = '%s' or xchan_epubkey = '%s'",
                    dbesc($portable_id),
                    dbesc(str_replace('did:key:', '', $portable_id))
                );

                if ($x) {
                    $xchans = q(
                        "select xchan_hash from xchan where xchan_hash = '%s' OR ( xchan_guid = '%s' AND xchan_pubkey = '%s' ) ",
                        dbesc($sigdata['portable_id']),
                        dbesc($x[0]['xchan_guid']),
                        dbesc($x[0]['xchan_pubkey'])
                    );

                    if ($xchans) {
                        $hashes = ids_to_querystr($xchans, 'xchan_hash', true);
                        $i = q(
                            "select id as item_id, item_private, uid from item where mid = '%s' $item_normal and owner_xchan in ( " . protect_sprintf($hashes) . " ) limit 1",
                            dbesc($test[0]['parent_mid'])
                        );
                    }
                }
            }

            // if we don't have a parent id belonging to the signer see if we can obtain one as a visitor that we have permission to access
            // with a bias towards those items owned by channels on this site (item_wall = 1)

            $sql_extra = item_permissions_sql(0, $portable_id);

            if (!$i) {
                $i = q(
                    "select id as item_id, item_private, uid from item where mid = '%s' $item_normal $sql_extra order by item_wall desc limit 1",
                    dbesc($test[0]['parent_mid'])
                );
            }

            if (!$i) {
                http_status_exit(403, 'Forbidden');
            }

            $parents_str = ids_to_querystr($i, 'item_id');

            $items = q(
                "SELECT item.*, item.id AS item_id FROM item WHERE item.parent IN ( %s ) $item_normal and item_private = %d and item_deleted = 0 and uid = %d",
                dbesc($parents_str),
                intval($i[0]['item_private']),
                intval($i[0]['uid'])
            );

            if (!$items) {
                http_status_exit(404, 'Not found');
            }

            xchan_query($items, true);
            $items = fetch_post_tags($items);

            $observer = App::get_observer();
            $parent = $items[0];

            $nitems = [];
            foreach ($items as $i) {
                $mids = [];

                if (intval($i['item_private'])) {
                    if (!$observer) {
                        continue;
                    }
                    // ignore private reshare, possibly from hubzilla
                    if ($i['verb'] === 'Announce') {
                        if (!in_array($i['thr_parent'], $mids)) {
                            $mids[] = $i['thr_parent'];
                        }
                        continue;
                    }
                    // also ignore any children of the private reshares
                    if (in_array($i['thr_parent'], $mids)) {
                        continue;
                    }

                    if ($observer['xchan_hash'] !== $i['owner_xchan']) {
                        if ((int)$parent['item_private'] !== (int)$i['item_private']) {
                            continue;
                        }
                    }
                }
                $nitems[] = $i;
            }

            if (!$nitems) {
                http_status_exit(404, 'Not found');
            }

            $channel = Channel::from_id($nitems[0]['uid']);

            if (!$channel) {
                http_status_exit(404, 'Not found');
            }

            if (!perm_is_allowed($channel['channel_id'], get_observer_hash(), 'view_stream')) {
                http_status_exit(403, 'Forbidden');
            }

            if ($contextHistory) {
                $i = ZlibActivity::encode_activity_collection($nitems, 'conversation/history/' . $item_id, 'OrderedCollection', true, $channel, count($nitems));
            }
            else {
                $i = ZlibActivity::encode_item_collection($nitems, 'conversation/' . $item_id, 'OrderedCollection', true, $channel, count($nitems));

            }
            if ($portable_id && (!intval($items[0]['item_private']))) {
                ThreadListener::store(z_root() . '/activity/' . $item_id, $portable_id);
            }

            if (!$i) {
                http_status_exit(404, 'Not found');
            }

            as_return_and_die($i, $channel);
        }

        goaway(z_root() . '/item/' . argv(1));
    }
}

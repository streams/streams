<?php

namespace Code\Module;

use App;
use Code\Web\Controller;
use Code\Web\Router;
use Code\Web\WebServer;

class Nomad_gateway extends Controller
{

    protected $module;

    public function init()
    {

        $url = null;
        for ($index = 1; $index < argc(); $index ++) {
            if ($index != 1) {
                $url .= '/';
            }
            $url .= argv($index);
        }
        // Extract the portable_id from the URL.
        $key = $url;
        $key = rtrim($key, '/');
        $index = strpos($key, '/');
        $key = substr($key, 0, $index ?: null);

        // Find a channel on this site which has that ed25519 key.
        $query = q("select * from xchan left join channel 
            on xchan_hash = channel_hash where xchan_hash = '%s'",
            dbesc($key)
        );
        if (!($query && isset($query[0]['channel_id']))) {
            http_status_exit(404, 'Not found');
        }
        $mappedPath = $this->mapObject(str_replace($key, '', rtrim($url, '/')), $query[0]);
        $localPath = ltrim($mappedPath, '/');
        App::$cmd = $localPath;
        App::$argv = explode('/', App::$cmd);

        App::$argc = count(App::$argv);
        if ((array_key_exists('0', App::$argv)) && strlen(App::$argv[0])) {
            if (strpos(App::$argv[0],'.')) {
                $_REQUEST['module_format'] = substr(App::$argv[0],strpos(App::$argv[0], '.') + 1);
                App::$argv[0] =  substr(App::$argv[0], 0, strpos(App::$argv[0], '.'));
            }

            App::$module = str_replace(".", "_", App::$argv[0]);
            App::$module = str_replace("-", "_", App::$module);
            if (str_starts_with(App::$module, '_')) {
                App::$module = substr(App::$module, 1);
            }
        }
        else {
            App::$argc = 1;
            App::$argv = ['home'];
            App::$module = 'home';
        }
        header('Link: ' . '<'  . $url . '>; rel="alternate"', false);
        // recursively call the router.
        App::$module_loaded = false;
        $router = new Router();
        $router->Dispatch();
        $webserver = new WebServer();
        $webserver->set_homebase();

        // now that we've been through the module content, see if the page reported
        // a permission problem via session based notifications and if so, a 403
        // response would seem to be in order.

        if (is_array($_SESSION['sysmsg']) && stristr(implode("", $_SESSION['sysmsg']), t('Permission denied'))) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 403 ' . t('Permission denied.'));
        }

        construct_page();

        killme();
    }

    protected function mapObject($path, $channel)
    {
        // lookup abstract paths
        $systemPaths = [
            '' => '/channel/' . $channel['channel_address'],
            '/inbox' => '/inbox/' . $channel['channel_address'],
            '/outbox' => '/outbox/' . $channel['channel_address'],
            '/followers' =>	'/followers/' . $channel['channel_address'],
            '/following' =>	'/following/' . $channel['channel_address'],
            '/permissions' => '/permissions/' . $channel['channel_address'],
            '/actor' => '/channel/' . $channel['channel_address'],
            '/actor/inbox' => '/inbox/' . $channel['channel_address'],
            '/actor/outbox' => '/outbox/' . $channel['channel_address'],
            '/actor/followers' =>	'/followers/' . $channel['channel_address'],
            '/actor/following' =>	'/following/' . $channel['channel_address'],
            '/actor/permissions' => '/permissions/' . $channel['channel_address'],
        ];

        $partialPaths = [
            '/files/' => '/cloud/' . $channel['channel_address'],
            '/photos/' => '/photos/' . $channel['channel_address'],
            '/album/' => '/album/' . $channel['channel_address'],
            '/object/' => '/item',
        ];

        foreach ($systemPaths as $index => $localPath) {
            if ($path === $index) {
                return $localPath;
            }
        }

        foreach ($partialPaths as $index => $localPath) {
            if (str_starts_with($path, $index)) {
                $suffix = substr($path, strlen($index));
                return $localPath . '/' . ltrim($suffix, '/');
            }
        }
        return $path;
    }


}

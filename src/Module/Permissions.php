<?php

namespace Code\Module;

use Code\Lib as Zlib;
use Code\Lib\Activity;
use Code\Lib\ActivityStreams;
use Code\Lib\Channel;
use Code\Lib\Config;
use Code\Lib\Libzot;
use Code\Web\Controller;
use Code\Web\HTTPSig;

class Permissions extends Controller
{
    public function init()
    {
        // Handle ActivityPub and Nomad responses
        if (ActivityStreams::is_as_request() || Libzot::is_nomad_request()) {

            // Get the target channel from the URL.

            if (argc() > 1) {
                $channel = Channel::from_username(argv(1));
            } else {
                $channel = Channel::get_system();
            }

            // Get the source channel from the signature

            $sigdata = HTTPSig::verify(EMPTY_STR);
            if ($sigdata['portable_id'] && $sigdata['header_valid']) {
                $portable_id = $sigdata['portable_id'];
                if (!check_channelallowed($portable_id)) {
                    http_status_exit(403, 'Permission denied');
                }
                if (!check_siteallowed($sigdata['signer'])) {
                    http_status_exit(403, 'Permission denied');
                }
                observer_auth($portable_id);
            }
            elseif (Config::Get('system', 'require_authenticated_fetch', false)) {
                http_status_exit(403, 'Permission denied');
            }

            $observer_hash = get_observer_hash();

            // Get and return the permissions granted to source
            // by the target channel.

            $myPerms = get_all_perms($channel['channel_id'], $observer_hash);
            $concise_perms = [];
            if ($myPerms) {
                foreach ($myPerms as $k => $v) {
                    if ($v) {
                        $concise_perms[] = $k;
                    }
                }
                $myPerms = $concise_perms;
            }
            $isNomadic = Zlib\PConfig::Get($channel['channel_id'], 'system', 'nomadicAP');
            // convert to  ActivityStreams Collection object
            $ret = Activity::encode_simple_collection(Activity::map_permissions($myPerms),
                (($isNomadic) ? Channel::getDidResolver($channel) . '/permissions' : z_root() . '/permissions/' . $channel['channel_address']), 'Collection',
                count($myPerms), ['attributedTo' => Channel::getDidResolver($channel, true), 'collectionOf' => 'nomad:permissions']);
            // Return it.
            as_return_and_die($ret, $channel);
        }
        // No content is available at this URL endpoint for HTML requests.
        http_status_exit(404, 'Not found.');
    }

}
